# fibapix-covid-tracker-frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## All dependencies
```
npm install -g @vue/cli
vue add vuetify
vue add router
npm install vue-morris --save
npm install jquery --save
npm install vuelidate --save
npm install axios --save
npm install ramda --save
```

## Node js version
```
14.16.0
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
