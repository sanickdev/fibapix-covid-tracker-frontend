import axios from "axios";

const HTTP = axios.create( {
  //baseURL : `http://localhost:9089/`
 } );

HTTP.interceptors.response.use(
  function (response) {
    //handleResponse(response);
    return response;
  }, function (error) {
    //handleError(error);
    return Promise.reject(error);
  }
);

export { HTTP };