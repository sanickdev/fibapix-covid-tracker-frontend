import Vue from 'vue'
import VueRouter from 'vue-router'

const Home = '/home/Home';
const Feed = '/home/Feed';
const LogIn = '/authentication/LogIn';
//--------------------------------------
const Company = '/reports/company/Company'
const Hr = '/reports/hr/Hr'
const JobPerformance = '/reports/jobperformance/JobPerformance';
//--------------------------------------
const RegionsByTerritory = '/home/pages/RegionsByTerritory';
const StoresByRegion = '/home/pages/StoresByRegion'; 
//--------------------------------------
const Region = '/catalog/region/Region'
const Territory = '/catalog/territory/Territory'
const Store = '/catalog/store/Store'

Vue.use(VueRouter)

const lazyLoad = (routeComponent) =>{
  return () => import(`@/views${routeComponent}`)
}

const routes = [
    { 
        path: '/', component: lazyLoad(Home), 
        children:[
          { path: '' , name: 'Feed' , component: lazyLoad(Feed) },
          { path: 'company' , name: 'Company' , component: lazyLoad(Company) },
          { path: 'hr' , name: 'Hr' , component: lazyLoad(Hr) },
          { path: 'jobperformance' , name: 'JobPerformance' , component: lazyLoad(JobPerformance) },
          { path: 'regions/:idterritory/' , name: 'RegionsByTerritory' , component: lazyLoad(RegionsByTerritory) },
          { path: 'stores/:idregion/' , name: 'StoresByRegion' , component: lazyLoad(StoresByRegion) },
          { path: 'region' , name: 'Region' , component: lazyLoad(Region) },
          { path: 'territory' , name: 'Territory' , component: lazyLoad(Territory) },
          { path: 'store' , name: 'Store' , component: lazyLoad(Store) }
        ]
      },
      { path: '/login', name:"LogIn", component: lazyLoad(LogIn) }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
