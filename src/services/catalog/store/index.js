import { HTTP } from "@/helpers/http-common";

const getAll = () =>new Promise((resolve,reject)=>{
  HTTP.get(
      '/api/v1/covid-tracker/core/store'
    ).then(response=>{
      resolve(response);
    }).catch(error=>{
      reject(error);
    });
});
  
  const save = (store) =>new Promise((resolve,reject)=>{
      const { id, name, status } = store;
      const operation = { store: { id, name, status } } 
    HTTP
    .post('/api/v1/covid-tracker/core/store' , operation
    ).then(response=>{
      console.log(response)
      resolve(response);
    }).catch(error=>{
      reject(error.response);
    });
  });
  
  const update = (store) =>new Promise((resolve,reject)=>{
      const { id, name, status } = store;
      const operation = { store: { id, name, status } } 
    HTTP
    .put('/api/v1/covid-tracker/core/store' , operation
    ).then(response=>{
      resolve(response);
    }).catch(error=>{
      reject(error.response);
    });
  });
  
  const get = (store) =>new Promise((resolve,reject)=>{
      const { id } = store;
    HTTP.get(
      `/api/v1/covid-tracker/core/store/${id}`
    ).then(response=>{
      resolve(response);
    }).catch(error=>{
      reject(error);
    });
  });
  
  
  const disable = (store) =>new Promise((resolve,reject)=>{
    const { id } = store;
    HTTP
    .delete("/api/v1/covid-tracker/core/store/"
      , { params:{ id } }
    ).then(response=>{
      resolve(response);
    }).catch(error=>{
      reject(error.response);
    });
  });
  
  //Export methods
  export default {
    getAll
    , get
    , save
    , update
    , disable
  }
  