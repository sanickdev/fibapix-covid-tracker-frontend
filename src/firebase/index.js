// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDqA4fH2ynRt9iQ55IAlQ_jQrwSMsY0r_M",
  authDomain: "fibapix-covid-tracker-frontend.firebaseapp.com",
  projectId: "fibapix-covid-tracker-frontend",
  storageBucket: "fibapix-covid-tracker-frontend.appspot.com",
  messagingSenderId: "2605935217",
  appId: "1:2605935217:web:c7d6ff45bde6d8357b49d0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);