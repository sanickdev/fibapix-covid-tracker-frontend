const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const webpack = require('webpack')
module.exports = {
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:9090'
        , changeOrigin: true
        , secure: true
      }
    }
  }
  , lintOnSave: false
  , transpileDependencies: [
    'vuetify'
  ]
  , configureWebpack: {
    plugins:[
     new webpack.ProvidePlugin({
        $: 'jquery',
        jquery: 'jquery',
        'window.jQuery': 'jquery',
        jQuery: 'jquery'
      })
    ]
  }
}
